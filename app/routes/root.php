<?php

$app->get('/', function() use($app) {
  $datos = array(
    'titulo' => 'Ocupacion y Empleo',
    'recursos' => array(
      array('url' => '/api', 'descripcion' => 'Este documento HTML'),
      array('url' => '/api/entidades', 'descripcion' => 'Un documento XML'),
    )
  );
  $app->render('root.php', $datos);
});
