<!DOCTYPE html>
<head>
<meta charset="utf-8">
  <title> API </title>
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/bootstrap-responsive.css" rel="stylesheet">
</head>
  <ul>
    <li><a href="/api/entidades"> Entidades </a></li>
    <li><a href="/api/municipios"> Municipios </a></li>
    <li>Temas
    <ul>
    <li><a href="/api/temas_nivel_1"> temas_nivel_1 </a></li>
    <li><a href="/api/temas_nivel_2"> temas_nivel_2 </a></li>
    <li><a href="/api/temas_nivel_3"> temas_nivel_3 </a>

    </ul>
    </li>
    <li><a href="/api/indicadores"> Indicadores </a></li>
    </ul>
  </ul>
</body>
</html>